﻿using System;
using System.Collections.Generic;
using System.Media;
using System.Windows;
using System.Windows.Threading;

namespace Reloj
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private System.Timers.Timer timerClock = new System.Timers.Timer();

        public MainWindow()
        {
            InitializeComponent();
            hora_actual.Text = DateTime.Now.ToString("HH:mm:ss");
            DispatcherTimer timer = new DispatcherTimer(new TimeSpan(0, 0, 1),
            DispatcherPriority.Normal, delegate
            {
                this.hora_actual.Text = DateTime.Now.ToString("HH:mm:ss");
            }, this.Dispatcher);

         

        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Creador del programa: etc etc");
        }

        private void Salir_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

        private void Activar_Click(object sender, RoutedEventArgs e)


        {
            Alarma a1 = new Alarma();
            MessageBoxResult result = MessageBox.Show("¿Quieres activar la alarma?", "Clock", MessageBoxButton.OKCancel);
            switch (result)
            {
                case MessageBoxResult.OK:
                    MessageBox.Show("Alarma Activada", "Clock");
                    a1.activa = true;
                    a1.AlarmTime = Convert.ToDateTime(HoraAlarma.Text);
                    IniciarAlarma();
                    break;
                case MessageBoxResult.Cancel:
                    MessageBox.Show("Alarma Desactivada", "Clock");
                    a1.activa = false;
                    break;
            }
        }

        private void Desactivar_Click(object sender, RoutedEventArgs e)
        {
            Alarma a1 = new Alarma();
            MessageBoxResult result = MessageBox.Show("¿Quieres desactivar la alarma?", "Clock", MessageBoxButton.OKCancel);
            switch (result)
            {
                case MessageBoxResult.OK:
                    MessageBox.Show("Alarma Desactivada", "Clock");
                    a1.activa = false;
                    break;
                case MessageBoxResult.Cancel:
                    MessageBox.Show("Alarma Activada", "Clock");
                    a1.activa = true;
                    a1.AlarmTime = Convert.ToDateTime(HoraAlarma.Text);
                    break;
            }
        }

        private void IniciarAlarma()
        {
            Alarma a1 = new Alarma();
            if (a1.activa)
            {

                /*  DateTime timenow = Convert.ToDateTime(hora_actual.Text);

                  if  (a1.AlarmTime == timenow)
                 {
                SystemSounds.Beep.Play(); 
                MessageBox.Show("ALARMA");
                }
                */
            }
        }


        private void deletePais_Click(object sender, RoutedEventArgs e)
        {

        }

        private void addPais_Click(object sender, RoutedEventArgs e)
        {
            AddPaises ap = new AddPaises();
            ap.ShowDialog();
           
        }
    }

 }


