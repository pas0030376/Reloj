﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reloj
{
    [Serializable()]
    class Alarma
    {
        public Boolean activa { set; get; }
        public DateTime AlarmTime { set; get; }

    }
}
